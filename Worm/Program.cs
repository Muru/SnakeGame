﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;


namespace Worm
{
    class Pos
    {
        public int X;
        public int Y;
        public Pos(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    class Program
    {
        static List<Pos> SnakeBody = new List<Pos>();
        static Pos Food;
        static List<Pos> Poison = new List<Pos>();
        static int Points = 0;
        static Random r = new Random();


        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Border();
            Start();


           

        }

        static void Border()
        {
            for (int i = 0; i < Console.WindowWidth; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write("-");

                Console.SetCursorPosition(i, Console.WindowHeight-1);
                Console.Write("-");
            }
            for (int i = 0; i < Console.WindowHeight; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write("|");

                Console.SetCursorPosition(Console.WindowWidth-1, i);
                Console.Write("|");
            }
        }

        static void CreateFood()
        {
            Food = new Pos(r.Next(2, Console.WindowWidth - 2), r.Next(2, Console.WindowHeight - 2));
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.SetCursorPosition(Food.X, Food.Y);
            Console.Write("*");
            Console.SetCursorPosition(SnakeBody[SnakeBody.Count - 1].X, SnakeBody[SnakeBody.Count - 1].Y);
        }

        static void CreatePoison()
        {
            Poison.Add(new Pos(r.Next(4, Console.WindowWidth - 4), r.Next(4, Console.WindowHeight - 4)));
            foreach (var item in Poison)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.SetCursorPosition(item.X, item.Y);
                Console.Write("#");
            }          
            Console.SetCursorPosition(SnakeBody[SnakeBody.Count - 1].X, SnakeBody[SnakeBody.Count - 1].Y);
        }

        static void Start()
        {

            foreach (Pos p in Poison)
            {
                Console.SetCursorPosition(p.X, p.Y);
                Console.Write(" ");
            }
            Poison.Clear();
            Poison.Add(new Pos(0, 0));

            //create SnakeBody
            for (int i = 1; i < 4; i++)
            {
                SnakeBody.Add(new Pos(i + 1, 3));
            }

            foreach (Pos num in SnakeBody)
            {
                Console.SetCursorPosition(num.X, num.Y);
                Console.Write("0");
            }

            CreateFood();
            CreatePoison();

            ConsoleKeyInfo keyInfo;
            while ((keyInfo = Console.ReadKey(true)).Key != ConsoleKey.Escape)
            {
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        SnakeBody.Add(new Pos(SnakeBody[SnakeBody.Count - 1].X, SnakeBody[SnakeBody.Count - 1].Y - 1));
                        MoveSnake();
                        break;
                    case ConsoleKey.DownArrow:
                        SnakeBody.Add(new Pos(SnakeBody[SnakeBody.Count - 1].X, SnakeBody[SnakeBody.Count - 1].Y + 1));
                        MoveSnake();
                        break;
                    case ConsoleKey.RightArrow:
                        SnakeBody.Add(new Pos(SnakeBody[SnakeBody.Count - 1].X + 1, SnakeBody[SnakeBody.Count - 1].Y));
                        MoveSnake();
                        break;
                    case ConsoleKey.LeftArrow:
                        SnakeBody.Add(new Pos(SnakeBody[SnakeBody.Count - 1].X - 1, SnakeBody[SnakeBody.Count - 1].Y));
                        MoveSnake();
                        break;
                }
            }
        }

        static void GameOver()
        {
            

            Points = SnakeBody.Count-3;
            foreach (Pos num in SnakeBody)
            {
                Console.SetCursorPosition(num.X, num.Y);
                Console.Write(" ");
            }
            SnakeBody.Clear();
            SnakeBody.Add(new Pos(1, 3));

            Console.SetCursorPosition(8, 45);
            Console.Write($"GAME OVER    --YOU GOT  {Points}  POINTS--      Push Enter to start again...");

            ConsoleKeyInfo keyInfo;
            while ((keyInfo = Console.ReadKey(true)).Key != ConsoleKey.Escape)
            {
                switch (keyInfo.Key)
                {
                    case ConsoleKey.Enter:
                        Start();
                        break;
                }
            }
        }

        static void MoveSnake()
        {
            //remove the tail and add head
            //remove first element of SnakeBody and write 0 to added Pos
            Console.SetCursorPosition(SnakeBody[0].X, SnakeBody[0].Y);
            Console.Write(" ");

            SnakeBody.Remove(SnakeBody[0]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(SnakeBody[SnakeBody.Count - 1].X, SnakeBody[SnakeBody.Count - 1].Y);
            Console.Write("0");

            //when Snakes head is same Pos that Food
            if (SnakeBody[SnakeBody.Count - 1].X == Food.X && SnakeBody[SnakeBody.Count - 1].Y == Food.Y)
            {
                SnakeBody.Add(new Pos(SnakeBody[SnakeBody.Count - 1].X, SnakeBody[SnakeBody.Count - 1].Y +1));
                Console.SetCursorPosition(SnakeBody[SnakeBody.Count - 1].X, SnakeBody[SnakeBody.Count - 1].Y);
                Console.Write("0");
                CreateFood();
                CreatePoison();
            }

            //when Snakes head is same Pos that Poison
            foreach (var p in Poison)
            {
                if (SnakeBody[SnakeBody.Count - 1].X == p.X && SnakeBody[SnakeBody.Count - 1].Y == p.Y)
                {
                    GameOver();
                }
            }
           
            //when Snake head hites the window walls
            if (SnakeBody[SnakeBody.Count - 1].X <= 0 || SnakeBody[SnakeBody.Count - 1].X >= Console.WindowWidth-1)
            {
                GameOver();
            }
            if (SnakeBody[SnakeBody.Count - 1].Y <= 0 || SnakeBody[SnakeBody.Count - 1].Y >= Console.WindowHeight-1)
            {
                GameOver();
            }
        }

    }

}
